import network
from micropyserver import MicroPyServer
from machine import Pin

wlan_id = "alexxx"
wlan_pass = "peston53"
myPin = Pin(2, Pin.OUT)

wlan = network.WLAN(network.STA_IF)
wlan.active(True)

while not wlan.isconnected():
    wlan.connect(wlan_id, wlan_pass)
print("Connected... IP: " + wlan.ifconfig()[0])  


''' there should be a wi-fi connection code here '''

def hello_world(request):
    ''' request handler '''
    server.send("HELLO WORLD!")
    
def switch_on(request):
    server.send("Switch on")
    myPin.off()
    
def switch_off(request):
    server.send("Switch off")
    myPin.on()

server = MicroPyServer()
''' add route '''
server.add_route("/", hello_world)
server.add_route("/switch_on", switch_on)
server.add_route("/switch_off", switch_off)

''' start server '''
server.start()

